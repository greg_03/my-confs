#!/bin/bash

dot_files="bashrc gitconfig screenlayout xinitrc zshenv zshrc"

for i in $dot_files
do
    ln -s $(pwd)/$i $HOME/.$i
done

config_dir="alacritty git i3status i3 nvim redshift.conf wallpapers zsh"

for i in $config_dir
do
    ln -s $(pwd)/$i $HOME/.config/$i 
done
