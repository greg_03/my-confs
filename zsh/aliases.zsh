#File with my own aliases

# some more ls aliases
alias ls='ls --color=auto'
alias ll='ls -aFlh'
alias la='ls -A'
alias l='ls -CF'
alias lh='ls -sh'

alias dir='dir --color=auto'
alias vdir='vdir --color=auto'

alias grep='grep --color=auto'
alias fgrep='fgrep --color=auto'
alias egrep='egrep --color=auto'

alias rm='rm -v --preserve-root'
alias mkdir='mkdir -vp'
alias cp='cp -iv'
alias py='python3'
alias grep='grep -in'
alias mv='mv -v'
alias ssh='ssh -X'
alias mysql='mysql -p --default-character-set=utf8 --auto-rehash'
alias mount='udisksctl mount -b'
alias umount='udisksctl unmount -b'
alias lsblk='lsblk -o NAME,TYPE,MOUNTPOINTS,SIZE,FSUSED,FSUSE%'
alias dd='dd status=progress'
alias free='free -h'
alias vim='nvim'
alias lock='loginctl lock-session'

alias make='make -j8'
alias pytest='pytest -v --disable-warnings'

# Create py venv
alias pyv='py -m venv ./venv'

# Cargo aliases
alias c='cargo'
