set nocompatible
filetype off

" Vundle for plug-in managment
" All plug in
call plug#begin()
Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'vim-airline/vim-airline'
Plug 'frazrepo/vim-rainbow'
Plug 'preservim/nerdtree'
Plug 'vim-scripts/Conque-GDB'
call plug#end()

filetype plugin indent on

" Make <CR> to accept selected completion item or notify coc.nvim to format
" <C-g>u breaks current undo, please make your own choice
inoremap <silent><expr> <CR> coc#pum#visible() ? coc#pum#confirm()
                              \: "\<C-g>u\<CR>\<c-r>=coc#on_enter()\<CR>"

" Map key to display coc action
vmap <leader>q <Plug>(coc-codeaction-selected)
nmap <leader>q <Plug>(coc-codeaction-selected)

" Preferences
syntax on
colorscheme default
set number
set relativenumber

"Display a symbol to see indent
" set list listchars=tab:-|
set linebreak

" j/k and arrows will move vitual lines
noremap <silent> <expr> j (v:count == 0 ? 'gj' : 'j')
noremap <silent> <expr> k (v:count == 0 ? 'gk' : 'k')
noremap <silent> <expr> <Up> (v:count == 0 ? 'gk' : 'k')
noremap <silent> <expr> <Down> (v:count == 0 ? 'gj' : 'j')

imap jk <Esc>
imap kj <Esc>
set mouse=a
set smartcase
set showcmd

" All stuff about indent
set smartindent
"set colorcolumn=80
set tabstop=4
set softtabstop=4
set expandtab " Transform a tabulation into spaces
set shiftwidth=4 " Make a tabulation 4 spaces
set smarttab " Make smart tabs
set smartindent " Automatically indent
set cindent " Special indentations for C language
set textwidth=80
set backspace=eol,start,indent " Backspace key remove the 4 spaces of an indent at once
set clipboard=unnamedplus
set belloff=all
set nohlsearch

" autocmd VimEnter * NERDTree

" Templates for different files
if has("autocmd")
	augroup templates
		autocmd BufNewFile *.c 0r ~/.config/nvim/templates/template.c
		autocmd BufNewFile *.h 0r ~/.config/nvim/templates/template.h
		autocmd BufNewFile *.cpp 0r ~/.config/nvim/templates/template.cpp
		autocmd BufNewFile *.html 0r ~/.config/nvim/templates/template.html
		autocmd BufNewFile *.php 0r ~/.config/nvim/templates/template.php
		autocmd BufNewFile *.hpp 0r ~/.config/nvim/templates/template.hpp
		autocmd BufNewFile *.py 0r ~/.config/nvim/templates/template.py
		autocmd BufNewFile *.sh 0r ~/.config/nvim/templates/template.sh
		autocmd BufNewFile Program.cs 0r ~/.config/nvim/templates/template_Program.cs
		autocmd BufNewFile *.cs 0r ~/.config/nvim/templates/template.cs
		autocmd BufNewFile *.tex 0r ~/.config/nvim/templates/template.tex
	augroup END
endif




" Make configuration
autocmd Filetype make setlocal noexpandtab

" per .git vim configs
" just `git config vim.settings "expandtab sw=4 sts=4"` in a git repository
" change syntax settings for this repository
"let git_settings = system("git config --get vim.settings")
"if strlen(git_settings)
"	exe "set" git_settings
"endif
